###
# Dockerfile to build booktype image
# Based on Debian Stable FTW
###

FROM debian:jessie
MAINTAINER alcalina@riseup.net

# Set env variables
ENV booktype_version 2.0

# Update && upgrade apt from repositories
RUN apt-get update && apt-get -y upgrade

# Install python related debian packages
RUN apt-get install -y python \
  python-dev \
  git-core \
  python-pip \
  python-virtualenv

# Install database related debian packages
RUN apt-get install -y sqlite3 \
  libpq-dev
  
# Install redis related debian packages
RUN apt-get install -y redis-server \ 
  libxml2-dev \
  libxslt-dev

# Install curl, git and unzip to fetch package
RUN apt-get install -y curl \
  unzip

# Create python virtual environment
#RUN virtualenv --distribute /booktype-virtualenv
#RUN #!/bin/bash . /booktype-virtualenv/bin/activate
#RUN cd /booktype-virtualenv

# Fetch Booktype package from github
RUN curl -SL https://github.com/sourcefabric/Booktype/archive/${booktype_version}.zip > booktype.zip
RUN unzip booktype.zip && rm booktype.zip
RUN ln -s Booktype-${booktype_version}/ /Booktype

# Install Python requirements
RUN pip install -r /Booktype/requirements/prod.txt
RUN pip uninstall -y celery
RUN pip install celery==3.1


# Cellery
RUN apt-get install -y supervisor

EXPOSE 8080
RUN "pwd"
COPY ./entrypoint.sh entrypoint.sh
CMD ["./entrypoint.sh"] 

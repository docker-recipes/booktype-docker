#!/bin/bash
# AGPL 3+ - alcalina

# datadir=${APP_DATADIR:="/var/lib/data"}
host=${BOOKTYPE_HOST:="0.0.0.0"}
port=${BOOKTYPE_PORT:="8080"}
username=${BOOKTYPE_USERNAME:="sysadmin"}
password=${BOOKTYPE_PASSWORD:=""}
database=${BOOKTYPE_DATABASE:=""}
admin_email=${BOOKTYPE_ADMIN_EMAIL:="test@example.org"}

mkdir -p /var/www/ && cd /var/www

if [ -z ${database} ];
  then echo 'No database name, so we will create an sqlite instance';
  /Booktype/scripts/createbooktype -p prod --check-versions --database sqlite www_booktype;
else 
  echo 'Install with postgresql'; 
  /Booktype/scripts/createbooktype -p prod --check-versions --database postgresql www_booktype;
fi

/var/www/www_booktype/manage.py migrate
/var/www/www_booktype/manage.py syncdb --noinput
/var/www/www_booktype/manage.py createsuperuser # --username=${username} --email=${admin_email} --noinput
/var/www/www_booktype/manage.py runserver ${host}:${port}
